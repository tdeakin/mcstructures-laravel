<?php

namespace App\Http\Controllers;

use App\Structure;
use Illuminate\Http\Request;

class BrowseController extends Controller
{
    public function category($category, $sortMethod = "latest")
    {
        $list_name = $this->sortingList[$sortMethod]['name'] . " in " . ucwords($category);
        $this->sortingList[$sortMethod]['selected'] = true;
        $sortMethod = $this->sortingList[$sortMethod]['key'];

        if(!array_key_exists($category, $this->categoryNameToId)){
            return null;
        }

        $categoryVal = $this->categoryNameToId[$category];
        $this->categoryMenuArray[$categoryVal]['selected'] = true;
        // get structures in the category we want, ordered by the requested method
        // a max of 30 on a page
        $results = Structure::where('category', $categoryVal)
            ->orderBy($sortMethod, 'desc')
            ->join('users', 'user_id', '=', 'users.id')// we also want userdata
            ->paginate(30);

        return view('list', [
            'category' => $category,
            'structure_list' => $results,
            'list_name' => $list_name,
            'selected_category' => $categoryVal,
            'selected_sort_method' => $sortMethod
        ]);

    }
}
