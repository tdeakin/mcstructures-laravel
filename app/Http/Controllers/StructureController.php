<?php

namespace App\Http\Controllers;

use App\Structure;
use Illuminate\Http\Request;

class StructureController extends Controller
{
    public function structure($structureUrl)
    {
        $structure = Structure::where('url', $structureUrl)->join('users', 'user_id', '=', 'users.id')->take(1);
       // var_dump($structure->get()->flatten()->toArray()[0]);

        return view('structure',[
            'current' => $structure->get()->flatten()->toArray()[0],
        ]);
    }
}
