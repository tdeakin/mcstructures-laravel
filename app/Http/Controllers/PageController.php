<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Structure;


class PageController extends Controller
{

    // index page
    public function home()
    {
        $start_time = microtime(true);
        // homepage query
        $homepage = Structure::orderBy('downloads', 'desc')->take(5)
            ->join('users', 'user_id', '=', 'users.id');

        // loop through categories
        for ($i = 0; $i < 6; $i++) {
            $category_sub_query = Structure::where('category', '=', $i)->take(4)
                ->join('users', 'user_id', '=', 'users.id');
            $homepage = $homepage->unionAll($category_sub_query);
        }
        // add latest structures
        $latest = Structure::latest('structures.created_at')->take(4)
            ->join('users', 'user_id', '=', 'users.id');
        $homepage = $homepage->unionAll($latest);
        // get the results and put them in an array

        $result = $homepage->get()->flatten()->toArray();

        // slice up the results for convenience
        $featured = array_slice($result, 0, 5);
        $latest = array_slice($result, 29, 5);
        // splitting this into chunks too
        $data = array_chunk(array_slice($result, 5, 24), 4);

        // display the stuff
        return view('index', [
            'featured' => $featured,
            'latest' => $latest,
            'data' => $data,
            'elapsed' => microtime(true) - $start_time,
        ]);

    }

    public function termsOfService()
    {

    }

    public function privacyPolicy()
    {

    }

    public function contact()
    {

    }


    public function logout()
    {
        Auth::logout();
        return redirect('/');

    }

}
