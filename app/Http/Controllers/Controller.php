<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $categoryNameToId = [
        "terrain" => 0,
        "art" => 1,
        "redstone" => 2,
        "detail" => 3,
        "other" => 4,
    ];

    protected $categoryIdToName;

    protected $categoryMenuArray = [
        0 => [
            "url" => "terrain",
            "name" => "Terrain",
            "selected" => false,
        ],
        1 => [
            "url" => "art",
            "name" => "Art",
            "selected" => false,
        ],
        2 => [
            "url" => "redstone",
            "name" => "Redstone",
            "selected" => false,
        ],
        3 => [
            "url" => "detail",
            "name" => "Detail",
            "selected" => false,
        ],
        4 => [
            "url" => "buildings",
            "name" => "Buildings",
            "selected" => false,
        ],
        5 => [
            "url" => "other",
            "name" => "Other",
            "selected" => false,
        ],

    ];

    protected $classList = [
        0 => [
            "value" => "builder",
            "name" => "Builder",
        ],
        1 => [
            "value" => "architect",
            "name" => "Architect",
        ],
        2 => [
            "value" => "engineer",
            "name" => "Engineer",
        ],
        3 => [
            "value" => "artist",
            "name" => "Artist",
        ]
    ];

    protected $sortingList = [
        "latest" => [
            "name" => "Latest",
            "value" => "latest",
            "key" => "structures.created_at",
            "selected" => false
        ],
        "updated" => [
            "name" => "Recently Updated",
            "value" => "updated",
            "key" => "structures.updated_at",
            "selected" => false
        ],
        "views" => [
            "name" => "Most Viewed",
            "value" => "views",
            "key" => "views",
            "selected" => false
        ],
        "downloads" => [
            "name" => "Most Downloaded",
            "value" => "downloads",
            "key" => "downloads",
            "selected" => false
        ],
        "likes" => [
            "name" => "Most Liked",
            "value" => "likes",
            "key" => "likes",
            "selected" => false
        ]
    ];

    protected $test = "foo";

    public function __construct()
    {
        // flip the array to get ids that give names
        $this->categoryIdToName = array_flip($this->categoryNameToId);
        // share data with all the views
        View::share([
            'categoryMenu' => $this->categoryMenuArray,
            'sorting_option' => $this->sortingList,
        ]);


    }


}
