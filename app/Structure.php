<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends \Eloquent
{
    protected $fillable = ['name', 'url', 'tags', 'category', 'description', 'image', 'user_id'];


    public function author()
    {
        return $this->belongsTo('User');
    }
}
