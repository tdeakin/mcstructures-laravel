<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('datetime', function ($expression) {
            return "<?php echo $expression->format('m/d/Y H:i'); ?>";
        });

        Blade::directive('lower', function ($text) {
            return "<?php echo strtolower($text);?>";
        });

        Blade::directive('upper', function($text){
            return "<?php echo strtoupper($text);?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
