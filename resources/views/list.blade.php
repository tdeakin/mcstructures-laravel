@extends('layouts.app')

@section('title', 'Structure')

@section('content')

    <div class="col-md-12">
        <h2 class="list-title">{{ $list_name }}</h2>
        <nav class="nav nav-pills">
            @foreach($sorting_option as $option)
                @if($option['key'] == $selected_sort_method)
                    <li class="nav-item">
                        <a href="/category/@lower($category)/{{ $option['value'] }}"
                           class="nav-link active">{{ $option['name'] }}</a></li>
                @else
                    <li class="nav-item"><a href="/category/@lower($category)/{{ $option['value'] }}"
                                            class="nav-link">{{ $option['name'] }}</a></li>
                @endif
            @endforeach
        </nav>
        <ul class="result-list">
            @foreach($structure_list as $structure)

                <li>
                    <img src="/img/{{ $structure['image'] }}" alt="{{ $structure['name'] }}"
                         class="img-thumb">
                    <div class="structure-details">
                        <h3 class="title"><a href="/structure/{{ $structure['url'] }}">{{ $structure['name'] }}</a>
                        </h3>
                        <ul class="list-unstyled">
                            <li>{{ $structure['views'] }} Views</li>
                            <li>{{ $structure['downloads'] }} Downloads</li>
                            <li>{{ $structure['likes'] }} Likes</li>
                        </ul>
                        <p>By <a href="/profile/{{ $structure['username'] }}">{{ $structure['username'] }}</a>
                        </p>
                    </div>
                </li>

            @endforeach
        </ul>
    </div>
    {{$structure_list->links()}}
@endsection