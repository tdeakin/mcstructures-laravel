<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Minecraft Structures - @yield('title')</title>
    <link href="/css/main.css" type="text/css" rel="stylesheet">
</head>
<body>
@include('partials.navbar')
<div class="container">
    @yield('content')
</div>

<footer>
    <a href="/">Home</a>
</footer>
<script
        src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="
        crossorigin="anonymous"></script>
<script src="/polyfill.object-fit.min.js"></script>
<script>
    objectFit.polyfill({
        selector: 'img', // this can be any CSS selector
        fittype: 'cover', // either contain, cover, fill or none
        disableCrossDomain: 'true' // either 'true' or 'false' to not parse external CSS files.
    });
</script>
<script src="/Wallop.min.js"></script>
<script>
    var wallopEl = document.querySelector('.Wallop');
    var slider = new Wallop(wallopEl);
</script>
<script src="/bootstrap.min.js"></script>
</body>
</html>