@extends('layouts.app')

@section('title', 'Structure')

@section('content')
    <div class="row">
        <div class="col-md-9">

            <div class="structure-box">
                <img class="img-main" src="../img/{{ $current['image'] }}" alt="{{ $current['name'] }}">
                <h2>{{ $current['name'] }}</h2>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <p>{!! $current['description']!!}</p>
                </div>
                <aside class="sidebar col-md-4">
                    <img class="img-thumb" src="{{ $current['image'] }}" alt="{{ $current['username'] }}">
                    <p class="username"><a href="/profile/{{ $current['username'] }}">{{ $current['username'] }}</a></p>
                    <p>Level {{ $current['level'] }} : {{ $current['title'] }}</p>
                    <h4>Download</h4>
                    <!--<a href="@{{ $current['file'] }}">Minecraft Structure File</a>-->

                    <h4>Statistics</h4>
                    <ul class="list-unstyled">
                        <li><span class="text-emphasis">{{ $current['views'] }}</span> Views</li>
                        <li><span class="text-emphasis">{{ $current['downloads'] }}</span> Downloads</li>
                        <li><span class="text-emphasis">{{ $current['likes'] }}</span> Likes</li>
                    </ul>
                </aside>

            </div>
        </div>
    </div>
@endsection