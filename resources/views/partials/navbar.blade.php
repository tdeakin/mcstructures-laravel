<header>
    <nav class="navbar navbar-dark">
        <div class="container">
            <div class="row">
                <h1 class="float-sm-none float-md-left">Minecraft Structures</h1>
                <nav class="float-sm-none float-md-right">
                    <ul class="nav navbar-nav">
                        <li class="nav-item"><a class="nav-link" href="/">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="">Browse</a></li>
                        @if(Auth::check())
                            <li class="nav-item"><a class="nav-link" href="/account/dashboard">{{ Auth::user()->username }}</a></li>
                        @else

                            <li class="nav-item"><a class="nav-link" href="/login">Log In</a>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </nav>
    @if($categoryMenu)
        <nav class="navbar navbar-light navbar-browse">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav navbar-nav">
                            @foreach($categoryMenu as $category)
                                @if(isset($selected_category) && $selected_category == $loop->index)
                                    <li class="nav-item active"><a class="nav-link active"
                                                                   href="/category/{{$category['url'] }}">{{$category['name'] }}</a>
                                    </li>
                                @else
                                    <li class="nav-item"><a class="nav-link"
                                                            href="/category/{{$category['url'] }}">{{$category['name'] }}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <form class="form-inline float-xs-right hidden-sm-down">
                            <input class="form-control" name="search" id="search" type="text" title="Search"
                                   placeholder="Search">
                            <button class="btn btn-primary" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </nav>
    @endif
</header>
