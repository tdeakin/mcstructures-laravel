@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="Wallop Wallop--fade">
                <div class="Wallop-list">
                    @foreach($featured as $sliderItem)
                        <div class="Wallop-item">
                            <img src="/img/{{ $sliderItem['image'] }}" class="slider-img"
                                 alt="{{ $sliderItem['name'] }}">
                            <div class="slider-box">
                                <p><a href="/structure/{{ $sliderItem['url'] }}">{{ $sliderItem['name'] }},
                                        by {{ $sliderItem['username'] }}</a></p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <button class="Wallop-buttonPrevious">Previous</button>
                <button class="Wallop-buttonNext">Next</button>
            </div>

            <h3>Latest Structures</h3>
            <div class="row">
                @foreach($latest as $structure)
                    <div class="col-md-3">
                        <img class="img-thumb" src="/img/{{$structure['image']}}" alt="{{$structure['name']}}">
                        <p><a href="/structure/{{$structure['url']}}">{{$structure['name']}}</a></p>
                        <p>
                            {{$structure['username']}}
                        </p>
                    </div>
                @endforeach
            </div>

            @foreach($data as $category)
                <h2> {{$categoryMenu[$loop->index]['name']}}</h2>
                <div class="row">
                    @foreach($category as $structure)
                        <div class="col-md-3">
                            <img class="img-thumb" src="/img/{{$structure['image']}}" alt="{{$structure['name']}}">
                            <p><a href="/structure/{{$structure['url']}}">{{$structure['name']}}</a></p>
                            <p>
                                {{$structure['username']}}
                            </p>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
@endsection