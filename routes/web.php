<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// pages
Route::get('/', 'PageController@home');
Route::get('terms-of-service', 'PageController@termsOfService');
Route::get('privacy-policy', 'PageController@privacyPolicy');

// display structure
Route::get('structure/{structureUrl}', 'StructureController@structure');

// browsing the site
Route::get('category/{category}/{sortMethod?}', 'BrowseController@category');
Route::get('search/', 'BrowseController@search');

// structure management



// profiles
Route::get('profile/{username}', 'UserController@profile');

// user management


Route::get('logout', 'PageController@logout');

Auth::routes();

